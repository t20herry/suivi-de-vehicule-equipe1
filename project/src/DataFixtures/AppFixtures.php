<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\FuelType;
use App\Entity\Model;
use App\Entity\UserBase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $admin = new UserBase();
        $admin->setEmail('admin@mail.com');
        $admin->setPassword(password_hash("admin", PASSWORD_BCRYPT));
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);

        $user = new UserBase();
        $user->setEmail('user@mail.com');
        $user->setPassword(password_hash("user", PASSWORD_BCRYPT));
        $user->setRoles(['ROLE_USER']);
        $manager->persist($user);

        $brand = new Brand();
        $brand->setName('brand1');
        $brand2 = new Brand();
        $brand2->setName('brand2');

        $manager->persist($brand);
        $manager->persist($brand2);

        $model = new Model();
        $model->setName('model1');
        $model->setBrand($brand);
        $model2 = new Model();
        $model2->setName('model2');
        $model2->setBrand($brand2);

        $manager->persist($model);
        $manager->persist($model2);

        $fuel = new FuelType();
        $fuel->setName('fuel1');

        $manager->persist($fuel);

        $manager->flush();
    }
}
