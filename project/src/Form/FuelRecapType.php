<?php

namespace App\Form;

use App\Entity\FuelRecap;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FuelRecapType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date')
            ->add('totalCounter')
            ->add('secondaryCounter')
            ->add('station')
            ->add('quantity')
            ->add('unitaryPrice')
            ->add('fuel')
            ->add('vehicle')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FuelRecap::class,
        ]);
    }
}
