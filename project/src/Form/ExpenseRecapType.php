<?php

namespace App\Form;

use App\Entity\ExpenseRecap;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpenseRecapType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date')
            ->add('totalCounter')
            ->add('garage')
            ->add('operationName')
            ->add('partsCost')
            ->add('laborCost')
            ->add('vehicle')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ExpenseRecap::class,
        ]);
    }
}
