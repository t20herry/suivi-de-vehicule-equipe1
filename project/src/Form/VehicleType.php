<?php

namespace App\Form;

use App\Entity\Vehicle;
use App\Repository\ModelRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VehicleType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('matricule')
            ->add('name')
            # the date filed should be in the past  
            ->add('acquisitionDate', null, [
                'years' => range(date('Y') - 20, date('Y')),
            ])
            ->add('fuel')
            ->add('model');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        # to resolve get the brand of the model and the user from the session
        $resolver->setDefaults([
            'data_class' => Vehicle::class,
        ]);
    }
}
