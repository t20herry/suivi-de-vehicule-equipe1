<?php

namespace App\Controller;

use App\Entity\ExpenseRecap;
use App\Form\ExpenseRecapType;
use App\Repository\ExpenseRecapRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/user/expense-recap')]
class ExpenseRecapController extends AbstractController
{
    #[Route('/', name: 'app_expense_recap_index', methods: ['GET'])]
    public function index(ExpenseRecapRepository $expenseRecapRepository): Response
    {
        return $this->render('expense_recap/index.html.twig', [
            'expense_recaps' => $expenseRecapRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_expense_recap_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ExpenseRecapRepository $expenseRecapRepository): Response
    {
        $expenseRecap = new ExpenseRecap();
        $form = $this->createForm(ExpenseRecapType::class, $expenseRecap);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $expenseRecapRepository->save($expenseRecap, true);

            return $this->redirectToRoute('app_expense_recap_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('expense_recap/new.html.twig', [
            'expense_recap' => $expenseRecap,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_expense_recap_show', methods: ['GET'])]
    public function show(ExpenseRecap $expenseRecap): Response
    {
        return $this->render('expense_recap/show.html.twig', [
            'expense_recap' => $expenseRecap,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_expense_recap_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ExpenseRecap $expenseRecap, ExpenseRecapRepository $expenseRecapRepository): Response
    {
        $form = $this->createForm(ExpenseRecapType::class, $expenseRecap);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $expenseRecapRepository->save($expenseRecap, true);

            return $this->redirectToRoute('app_expense_recap_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('expense_recap/edit.html.twig', [
            'expense_recap' => $expenseRecap,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_expense_recap_delete', methods: ['POST'])]
    public function delete(Request $request, ExpenseRecap $expenseRecap, ExpenseRecapRepository $expenseRecapRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$expenseRecap->getId(), $request->request->get('_token'))) {
            $expenseRecapRepository->remove($expenseRecap, true);
        }

        return $this->redirectToRoute('app_expense_recap_index', [], Response::HTTP_SEE_OTHER);
    }
}
