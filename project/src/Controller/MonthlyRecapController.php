<?php

namespace App\Controller;

use App\Entity\MonthlyRecap;
use App\Form\MonthlyRecapType;
use App\Repository\MonthlyRecapRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('user/monthly-recap')]
class MonthlyRecapController extends AbstractController
{
    #[Route('/', name: 'app_monthly_recap_index', methods: ['GET'])]
    public function index(MonthlyRecapRepository $monthlyRecapRepository): Response
    {
        return $this->render('monthly_recap/index.html.twig', [
            'monthly_recaps' => $monthlyRecapRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_monthly_recap_new', methods: ['GET', 'POST'])]
    public function new(Request $request, MonthlyRecapRepository $monthlyRecapRepository): Response
    {
        $monthlyRecap = new MonthlyRecap();
        $form = $this->createForm(MonthlyRecapType::class, $monthlyRecap);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $monthlyRecapRepository->save($monthlyRecap, true);

            return $this->redirectToRoute('app_monthly_recap_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('monthly_recap/new.html.twig', [
            'monthly_recap' => $monthlyRecap,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_monthly_recap_show', methods: ['GET'])]
    public function show(MonthlyRecap $monthlyRecap): Response
    {
        return $this->render('monthly_recap/show.html.twig', [
            'monthly_recap' => $monthlyRecap,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_monthly_recap_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, MonthlyRecap $monthlyRecap, MonthlyRecapRepository $monthlyRecapRepository): Response
    {
        $form = $this->createForm(MonthlyRecapType::class, $monthlyRecap);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $monthlyRecapRepository->save($monthlyRecap, true);

            return $this->redirectToRoute('app_monthly_recap_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('monthly_recap/edit.html.twig', [
            'monthly_recap' => $monthlyRecap,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_monthly_recap_delete', methods: ['POST'])]
    public function delete(Request $request, MonthlyRecap $monthlyRecap, MonthlyRecapRepository $monthlyRecapRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$monthlyRecap->getId(), $request->request->get('_token'))) {
            $monthlyRecapRepository->remove($monthlyRecap, true);
        }

        return $this->redirectToRoute('app_monthly_recap_index', [], Response::HTTP_SEE_OTHER);
    }
}
