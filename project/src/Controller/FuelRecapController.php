<?php

namespace App\Controller;

use App\Entity\FuelRecap;
use App\Form\FuelRecapType;
use App\Repository\FuelRecapRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/user/fuel-recap')]
class FuelRecapController extends AbstractController
{
    #[Route('/', name: 'app_fuel_recap_index', methods: ['GET'])]
    public function index(FuelRecapRepository $fuelRecapRepository): Response
    {
        return $this->render('fuel_recap/index.html.twig', [
            'fuel_recaps' => $fuelRecapRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_fuel_recap_new', methods: ['GET', 'POST'])]
    public function new(Request $request, FuelRecapRepository $fuelRecapRepository): Response
    {
        $fuelRecap = new FuelRecap();
        $form = $this->createForm(FuelRecapType::class, $fuelRecap);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fuelRecapRepository->save($fuelRecap, true);

            return $this->redirectToRoute('app_fuel_recap_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('fuel_recap/new.html.twig', [
            'fuel_recap' => $fuelRecap,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_fuel_recap_show', methods: ['GET'])]
    public function show(FuelRecap $fuelRecap): Response
    {
        return $this->render('fuel_recap/show.html.twig', [
            'fuel_recap' => $fuelRecap,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_fuel_recap_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, FuelRecap $fuelRecap, FuelRecapRepository $fuelRecapRepository): Response
    {
        $form = $this->createForm(FuelRecapType::class, $fuelRecap);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fuelRecapRepository->save($fuelRecap, true);

            return $this->redirectToRoute('app_fuel_recap_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('fuel_recap/edit.html.twig', [
            'fuel_recap' => $fuelRecap,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_fuel_recap_delete', methods: ['POST'])]
    public function delete(Request $request, FuelRecap $fuelRecap, FuelRecapRepository $fuelRecapRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $fuelRecap->getId(), $request->request->get('_token'))) {
            $fuelRecapRepository->remove($fuelRecap, true);
        }

        return $this->redirectToRoute('app_fuel_recap_index', [], Response::HTTP_SEE_OTHER);
    }
}
