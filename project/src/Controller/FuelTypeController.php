<?php

namespace App\Controller;

use App\Entity\FuelType;
use App\Form\FuelTypeType;
use App\Repository\FuelTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/fuel-type')]
class FuelTypeController extends AbstractController
{
    #[Route('/', name: 'app_fuel_type_index', methods: ['GET'])]
    public function index(FuelTypeRepository $fuelTypeRepository): Response
    {
        return $this->render('fuel_type/index.html.twig', [
            'fuel_types' => $fuelTypeRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_fuel_type_new', methods: ['GET', 'POST'])]
    public function new(Request $request, FuelTypeRepository $fuelTypeRepository): Response
    {
        $fuelType = new FuelType();
        $form = $this->createForm(FuelTypeType::class, $fuelType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fuelTypeRepository->save($fuelType, true);

            return $this->redirectToRoute('app_fuel_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('fuel_type/new.html.twig', [
            'fuel_type' => $fuelType,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_fuel_type_show', methods: ['GET'])]
    public function show(FuelType $fuelType): Response
    {
        return $this->render('fuel_type/show.html.twig', [
            'fuel_type' => $fuelType,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_fuel_type_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, FuelType $fuelType, FuelTypeRepository $fuelTypeRepository): Response
    {
        $form = $this->createForm(FuelTypeType::class, $fuelType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fuelTypeRepository->save($fuelType, true);

            return $this->redirectToRoute('app_fuel_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('fuel_type/edit.html.twig', [
            'fuel_type' => $fuelType,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_fuel_type_delete', methods: ['POST'])]
    public function delete(Request $request, FuelType $fuelType, FuelTypeRepository $fuelTypeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$fuelType->getId(), $request->request->get('_token'))) {
            $fuelTypeRepository->remove($fuelType, true);
        }

        return $this->redirectToRoute('app_fuel_type_index', [], Response::HTTP_SEE_OTHER);
    }
}
