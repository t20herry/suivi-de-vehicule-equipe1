<?php

// return a page with welcome for the root path
// src/Controller/WelcomeController.php
namespace App\Controller;

use App\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class WelcomeController extends AbstractController
{
    public function welcomeRedirection(): Response
    {
        // if no user is logged in, redirect to the login page
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        // if role is ROLE_ADMIN, redirect to the admin page
        if ($this->getUser()->getRoles()[0] === 'ROLE_ADMIN') {
            return $this->redirectToRoute('app_admin');
        }

        // else, redirect to the user page
        return $this->redirectToRoute('app_user');
    }
    #[Route('/', name: 'welcome')]
    public function index(): Response
    {
        return $this->welcomeRedirection();
    }
}
