<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LogoutController extends AbstractController
{
    public function logout(Security $security): Response
    {
        $security->logout();
        return $this->redirectToRoute('app_login');
    }
    #[Route('/logout', name: 'app_logout')]
    public function index()
    {
        # blank this is handle by the security.yaml
    }
}
