<?php

namespace App\Entity;

use App\Repository\FuelRecapRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FuelRecapRepository::class)]
class FuelRecap
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column]
    private ?int $totalCounter = null;

    #[ORM\Column]
    private ?int $secondaryCounter = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?FuelType $fuel = null;

    #[ORM\Column(length: 255)]
    private ?string $station = null;

    #[ORM\Column]
    private ?float $quantity = null;

    #[ORM\Column]
    private ?float $unitaryPrice = null;

    #[ORM\ManyToOne(inversedBy: 'fuelRecaps')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Vehicle $vehicle = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTotalCounter(): ?int
    {
        return $this->totalCounter;
    }

    public function setTotalCounter(int $totalCounter): self
    {
        $this->totalCounter = $totalCounter;

        return $this;
    }

    public function getSecondaryCounter(): ?int
    {
        return $this->secondaryCounter;
    }

    public function setSecondaryCounter(int $secondaryCounter): self
    {
        $this->secondaryCounter = $secondaryCounter;

        return $this;
    }

    public function getFuel(): ?FuelType
    {
        return $this->fuel;
    }

    public function setFuel(?FuelType $fuel): self
    {
        $this->fuel = $fuel;

        return $this;
    }

    public function getStation(): ?string
    {
        return $this->station;
    }

    public function setStation(string $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getUnitaryPrice(): ?float
    {
        return $this->unitaryPrice;
    }

    public function setUnitaryPrice(float $unitaryPrice): self
    {
        $this->unitaryPrice = $unitaryPrice;

        return $this;
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }
}
