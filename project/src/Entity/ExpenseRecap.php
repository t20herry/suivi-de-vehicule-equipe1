<?php

namespace App\Entity;

use App\Repository\ExpenseRecapRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExpenseRecapRepository::class)]
class ExpenseRecap
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column]
    private ?int $totalCounter = null;

    #[ORM\Column(length: 255)]
    private ?string $garage = null;

    #[ORM\Column(length: 255)]
    private ?string $operationName = null;

    #[ORM\Column]
    private ?float $partsCost = null;

    #[ORM\Column]
    private ?float $laborCost = null;

    #[ORM\ManyToOne(inversedBy: 'expenseRecaps')]
    private ?Vehicle $vehicle = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTotalCounter(): ?int
    {
        return $this->totalCounter;
    }

    public function setTotalCounter(int $totalCounter): self
    {
        $this->totalCounter = $totalCounter;

        return $this;
    }

    public function getGarage(): ?string
    {
        return $this->garage;
    }

    public function setGarage(string $garage): self
    {
        $this->garage = $garage;

        return $this;
    }

    public function getOperationName(): ?string
    {
        return $this->operationName;
    }

    public function setOperationName(string $operationName): self
    {
        $this->operationName = $operationName;

        return $this;
    }

    public function getPartsCost(): ?float
    {
        return $this->partsCost;
    }

    public function setPartsCost(float $partsCost): self
    {
        $this->partsCost = $partsCost;

        return $this;
    }

    public function getLaborCost(): ?float
    {
        return $this->laborCost;
    }

    public function setLaborCost(float $laborCost): self
    {
        $this->laborCost = $laborCost;

        return $this;
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }
}
