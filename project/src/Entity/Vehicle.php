<?php

namespace App\Entity;

use App\Repository\VehicleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VehicleRepository::class)]
class Vehicle
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $matricule = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $name = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?FuelType $fuel = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $acquisitionDate = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Model $model = null;

    #[ORM\OneToMany(mappedBy: 'vehicle', targetEntity: MonthlyRecap::class, orphanRemoval: true)]
    private Collection $monthlyRecaps;

    #[ORM\ManyToOne(inversedBy: 'vehicles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?UserBase $owner = null;

    #[ORM\OneToMany(mappedBy: 'vehicle', targetEntity: FuelRecap::class, orphanRemoval: true)]
    private Collection $fuelRecaps;

    #[ORM\OneToMany(mappedBy: 'vehicle', targetEntity: ExpenseRecap::class)]
    private Collection $expenseRecaps;

    public function __construct()
    {
        $this->monthlyRecaps = new ArrayCollection();
        $this->fuelRecaps = new ArrayCollection();
        $this->expenseRecaps = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatricule(): ?int
    {
        return $this->matricule;
    }

    public function setMatricule(int $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getModel(): ?Model
    {
        return $this->model;
    }

    public function setModel(?Model $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getFuel(): ?FuelType
    {
        return $this->fuel;
    }

    public function setFuel(?FuelType $fuel): self
    {
        $this->fuel = $fuel;

        return $this;
    }

    public function getAcquisitionDate(): ?\DateTimeInterface
    {
        return $this->acquisitionDate;
    }

    public function setAcquisitionDate(\DateTimeInterface $acquisitionDate): self
    {
        $this->acquisitionDate = $acquisitionDate;

        return $this;
    }

    /**
     * @return Collection<int, MonthlyRecap>
     */
    public function getMonthlyRecaps(): Collection
    {
        return $this->monthlyRecaps;
    }

    public function addMonthlyRecap(MonthlyRecap $monthlyRecap): self
    {
        if (!$this->monthlyRecaps->contains($monthlyRecap)) {
            $this->monthlyRecaps->add($monthlyRecap);
            $monthlyRecap->setVehicle($this);
        }

        return $this;
    }

    public function removeMonthlyRecap(MonthlyRecap $monthlyRecap): self
    {
        if ($this->monthlyRecaps->removeElement($monthlyRecap)) {
            // set the owning side to null (unless already changed)
            if ($monthlyRecap->getVehicle() === $this) {
                $monthlyRecap->setVehicle(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?UserBase
    {
        return $this->owner;
    }

    public function setOwner(?UserBase $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection<int, FuelRecap>
     */
    public function getFuelRecaps(): Collection
    {
        return $this->fuelRecaps;
    }

    public function addFuelRecap(FuelRecap $fuelRecap): self
    {
        if (!$this->fuelRecaps->contains($fuelRecap)) {
            $this->fuelRecaps->add($fuelRecap);
            $fuelRecap->setVehicle($this);
        }

        return $this;
    }

    public function removeFuelRecap(FuelRecap $fuelRecap): self
    {
        if ($this->fuelRecaps->removeElement($fuelRecap)) {
            // set the owning side to null (unless already changed)
            if ($fuelRecap->getVehicle() === $this) {
                $fuelRecap->setVehicle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ExpenseRecap>
     */
    public function getExpenseRecaps(): Collection
    {
        return $this->expenseRecaps;
    }

    public function addExpenseRecap(ExpenseRecap $expenseRecap): self
    {
        if (!$this->expenseRecaps->contains($expenseRecap)) {
            $this->expenseRecaps->add($expenseRecap);
            $expenseRecap->setVehicle($this);
        }

        return $this;
    }

    public function removeExpenseRecap(ExpenseRecap $expenseRecap): self
    {
        if ($this->expenseRecaps->removeElement($expenseRecap)) {
            // set the owning side to null (unless already changed)
            if ($expenseRecap->getVehicle() === $this) {
                $expenseRecap->setVehicle(null);
            }
        }

        return $this;
    }


    public function __toString(): string
    {
        return $this->name;
    }
}
