<?php

namespace App\Entity;

use App\Repository\MonthlyRecapRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MonthlyRecapRepository::class)]
class MonthlyRecap
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column]
    private ?int $totalCounter = null;

    #[ORM\Column]
    private ?int $secondaryCounter = null;

    #[ORM\ManyToOne(inversedBy: 'monthlyRecaps')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Vehicle $vehicle = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTotalCounter(): ?int
    {
        return $this->totalCounter;
    }

    public function setTotalCounter(int $totalCounter): self
    {
        $this->totalCounter = $totalCounter;

        return $this;
    }

    public function getSecondaryCounter(): ?int
    {
        return $this->secondaryCounter;
    }

    public function setSecondaryCounter(int $secondaryCounter): self
    {
        $this->secondaryCounter = $secondaryCounter;

        return $this;
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }
}
