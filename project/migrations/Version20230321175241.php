<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230321175241 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE brand_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE expense_recap_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE fuel_recap_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE fuel_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE model_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE monthly_recap_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE vehicle_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE brand (id INT NOT NULL, name VARCHAR(150) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE expense_recap (id INT NOT NULL, date DATE NOT NULL, total_counter INT NOT NULL, garage VARCHAR(255) NOT NULL, operation_name VARCHAR(255) NOT NULL, parts_cost DOUBLE PRECISION NOT NULL, labor_cost DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE fuel_recap (id INT NOT NULL, fuel_id INT NOT NULL, vehicle_id INT NOT NULL, date DATE NOT NULL, total_counter INT NOT NULL, secondary_counter INT NOT NULL, station VARCHAR(255) NOT NULL, quantity DOUBLE PRECISION NOT NULL, unitary_price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_451A428A97C79677 ON fuel_recap (fuel_id)');
        $this->addSql('CREATE INDEX IDX_451A428A545317D1 ON fuel_recap (vehicle_id)');
        $this->addSql('CREATE TABLE fuel_type (id INT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE model (id INT NOT NULL, brand_id INT NOT NULL, name VARCHAR(150) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D79572D944F5D008 ON model (brand_id)');
        $this->addSql('CREATE TABLE monthly_recap (id INT NOT NULL, vehicle_id INT NOT NULL, date DATE NOT NULL, total_counter INT NOT NULL, secondary_counter INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EC22C4A8545317D1 ON monthly_recap (vehicle_id)');
        $this->addSql('CREATE TABLE vehicle (id INT NOT NULL, brand_id INT NOT NULL, fuel_id INT NOT NULL, model_id INT NOT NULL, owner_id INT NOT NULL, matricule INT NOT NULL, name VARCHAR(100) DEFAULT NULL, acquisition_date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1B80E48644F5D008 ON vehicle (brand_id)');
        $this->addSql('CREATE INDEX IDX_1B80E48697C79677 ON vehicle (fuel_id)');
        $this->addSql('CREATE INDEX IDX_1B80E4867975B7E7 ON vehicle (model_id)');
        $this->addSql('CREATE INDEX IDX_1B80E4867E3C61F9 ON vehicle (owner_id)');
        $this->addSql('ALTER TABLE fuel_recap ADD CONSTRAINT FK_451A428A97C79677 FOREIGN KEY (fuel_id) REFERENCES fuel_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fuel_recap ADD CONSTRAINT FK_451A428A545317D1 FOREIGN KEY (vehicle_id) REFERENCES vehicle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE model ADD CONSTRAINT FK_D79572D944F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE monthly_recap ADD CONSTRAINT FK_EC22C4A8545317D1 FOREIGN KEY (vehicle_id) REFERENCES vehicle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E48644F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E48697C79677 FOREIGN KEY (fuel_id) REFERENCES fuel_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E4867975B7E7 FOREIGN KEY (model_id) REFERENCES model (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E4867E3C61F9 FOREIGN KEY (owner_id) REFERENCES user_base (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE brand_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE expense_recap_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE fuel_recap_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE fuel_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE model_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE monthly_recap_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE vehicle_id_seq CASCADE');
        $this->addSql('ALTER TABLE fuel_recap DROP CONSTRAINT FK_451A428A97C79677');
        $this->addSql('ALTER TABLE fuel_recap DROP CONSTRAINT FK_451A428A545317D1');
        $this->addSql('ALTER TABLE model DROP CONSTRAINT FK_D79572D944F5D008');
        $this->addSql('ALTER TABLE monthly_recap DROP CONSTRAINT FK_EC22C4A8545317D1');
        $this->addSql('ALTER TABLE vehicle DROP CONSTRAINT FK_1B80E48644F5D008');
        $this->addSql('ALTER TABLE vehicle DROP CONSTRAINT FK_1B80E48697C79677');
        $this->addSql('ALTER TABLE vehicle DROP CONSTRAINT FK_1B80E4867975B7E7');
        $this->addSql('ALTER TABLE vehicle DROP CONSTRAINT FK_1B80E4867E3C61F9');
        $this->addSql('DROP TABLE brand');
        $this->addSql('DROP TABLE expense_recap');
        $this->addSql('DROP TABLE fuel_recap');
        $this->addSql('DROP TABLE fuel_type');
        $this->addSql('DROP TABLE model');
        $this->addSql('DROP TABLE monthly_recap');
        $this->addSql('DROP TABLE vehicle');
    }
}
