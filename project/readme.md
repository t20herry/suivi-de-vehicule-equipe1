# Php Project

## Installation

first check if technical requirements are met for [symphony](https://symfony.com/doc/current/setup.html#technical-requirements)
and for [docker](https://docs.docker.com/install/)

then run the following commands:

```bash
    # INSTALL TOOLS
    sudo apt install php-pdo php-pgsql npm -y
    # INSTALL DEPENDENCIES
    composer install
    # START DATABASE WITH DOCKER
    docker-compose up -d
    # SETUP DATABASE
    php bin/console doctrine:database:create
    php bin/console doctrine:schema:create
    php bin/console doctrine:migrations:migrate
    # ADD FIXTURES
    php bin/console doctrine:fixtures:load
    # NPM INSTALL
    npm install
```

an admin user is created with the following credentials:

    email: admin@mails.com
    password: admin

## Usage

```bash
    # START DOCKER (if not already running)
    docker-compose up -d
    # START SERVER
    symfony server:start
    # START NPM WATCH
    npm run watch
```

## Add or update entities

```bash
    # CREATE ENTITY
    php bin/console make:entity
    # CREATE MIGRATION
    php bin/console make:migration
    # UPDATE DATABASE
    php bin/console doctrine:migrations:migrate
```

```

For styling we used [tailwindcss](https://tailwindcss.com/) that why we need to run `npm run watch` to compile the css.
```


## Repartion of the work

Gestion des utilisateurs, l’authentification, et le service relevé mensuel
Kim Amaury
Gestion des véhicules et le service carnet de carburant
Gurvan Thomas
Partie Administration et le service carnet des dépenses
Adrien Cleo